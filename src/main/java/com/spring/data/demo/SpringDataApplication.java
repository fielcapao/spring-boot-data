package com.spring.data.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

import javax.persistence.*;

import lombok.Data;
import lombok.NoArgsConstructor;

@SpringBootApplication
public class SpringDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(EmployeeRepository employeeRepository, ManagerRepository managerRepository) {
		return args -> {
			Manager manager1 = managerRepository.save(new Manager("Manager 1"));
			Manager manager2 = managerRepository.save(new Manager("Manager 2"));
			employeeRepository.save(new Employee("Rodrigo", "Rodrigues", "Java Developer", manager1));
			employeeRepository.save(new Employee("Anna", "Cio", "Laywer", manager2));
			employeeRepository.save(new Employee("Slaw", "Polish", "developer", manager2));
		};
	}
}

@Entity
@Data
@NoArgsConstructor
class Employee {
	@Id @GeneratedValue
	Long id;
	String firstName;
	String lastName;
	String role;

	@ManyToOne
	Manager manager;

	public Employee(String firstName, String lastName, String role, Manager manager) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.role = role;
		this.manager = manager;
	}
}

@RepositoryRestResource
interface EmployeeRepository extends CrudRepository<Employee, Long> {
	List<Employee> findByFirstNameEqualsIgnoreCase(@Param("q") String firstName);
}

@Entity
@Data
@NoArgsConstructor
class Manager {
	@Id @GeneratedValue
	Long id;
	String name;

	@OneToMany(mappedBy = "manager")
	List<Employee> employees;

	public Manager(String name) {
		this.name = name;
	}
}

@RepositoryRestResource
interface ManagerRepository extends CrudRepository<Manager, Long> {
	List<Manager> findByEmployeesRoleContains(@Param("q") String role);
	@org.springframework.data.jpa.repository.Query("select m.employees from Manager m where lower(m.name) like lower(concat(?1, '%') )")
	List<Employee> findEmployeesByManagerName(@Param("q") String name);
}